package villagerdrop.handler;

import java.util.concurrent.ThreadLocalRandom;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import villagerdrop.init.CustomItems;
import villagerdrop.util.VillagerDropConfig;

class ItemWithAmount {
	public Item item;
	public int min;
	public int max;
	
	public ItemWithAmount(Item item, int min, int max) {
		this.item = item;
		this.min = min;
		this.max = max;
	}
}

public class DropHandler {
	public static final double RARE_ITEM_DROP_RATE = 0.2;
	
	// Adult villager rare items pool
	public static final ItemWithAmount[] ADULT_VILLAGER_RARE_ITEMS = new ItemWithAmount[] {
		new ItemWithAmount(CustomItems.VILLAGER_JUICE, 1, 2),
		new ItemWithAmount(CustomItems.VILLAGER_ENERGY_DRINK, 1, 2),
		new ItemWithAmount(CustomItems.VILLAGER_BEER, 1, 2),
		new ItemWithAmount(CustomItems.VILLAGER_WINE, 1, 2),
	};
	
	// Child villager rare items pool
	public static final ItemWithAmount[] CHILD_VILLAGER_RARE_ITEMS = new ItemWithAmount[] {
		new ItemWithAmount(CustomItems.VILLAGER_JUICE, 1, 2),
		new ItemWithAmount(CustomItems.VILLAGER_ENERGY_DRINK, 1, 2),
	};
	
	@SubscribeEvent
	public void dropHandler(LivingDropsEvent event) {
		// Check if the dropping entity is villager
		Entity entity = event.getEntity();
		if (!(entity instanceof EntityVillager)) {
			return;
		}
		EntityVillager villager = (EntityVillager) entity;
		
		// Check if config prevent player kill villager drop xp
		if(VillagerDropConfig.preventVillagerDropWhenKilledByPlayer && (event.getSource().getTrueSource() instanceof EntityPlayer)) {
			return;
		}
		
		// Check if the villager is not adult
		if(!villager.isChild()) {
			// XPOrb
			villager.getEntityWorld().spawnEntity(
					new EntityXPOrb(
							villager.getEntityWorld(), 
							villager.lastTickPosX, 
							villager.lastTickPosY, 
							villager.lastTickPosZ, 
							ThreadLocalRandom.current().nextInt(1, 4)
					)
			);
			
			// Emerald
			villager.dropItem(Items.EMERALD, ThreadLocalRandom.current().nextInt(0, 3));
			
			// Piece of Villager
			if(villager.isBurning()) {
				villager.dropItem(CustomItems.COOKED_PIECE_OF_VILLAGER, ThreadLocalRandom.current().nextInt(1, 4));
			} else {
				villager.dropItem(CustomItems.PIECE_OF_VILLAGER, ThreadLocalRandom.current().nextInt(1, 4));
			}
			
			// Rare item
			if(RARE_ITEM_DROP_RATE > ThreadLocalRandom.current().nextDouble()) {
				int rareItemIndex = ThreadLocalRandom.current().nextInt(ADULT_VILLAGER_RARE_ITEMS.length);
				ItemWithAmount itemWithAmount = ADULT_VILLAGER_RARE_ITEMS[rareItemIndex];
				villager.dropItem(itemWithAmount.item, ThreadLocalRandom.current().nextInt(itemWithAmount.min, itemWithAmount.max + 1));
			}
		} else {
			// Emerald
			villager.dropItem(Items.EMERALD, ThreadLocalRandom.current().nextInt(0, 2));
			
			// Piece of Villager
			if(villager.isBurning()) {
				villager.dropItem(CustomItems.COOKED_PIECE_OF_VILLAGER, ThreadLocalRandom.current().nextInt(0, 2));
			} else {
				villager.dropItem(CustomItems.PIECE_OF_VILLAGER, ThreadLocalRandom.current().nextInt(0, 2));
			}
			
			// Rare item
			if(RARE_ITEM_DROP_RATE > ThreadLocalRandom.current().nextDouble()) {
				int rareItemIndex = ThreadLocalRandom.current().nextInt(CHILD_VILLAGER_RARE_ITEMS.length);
				ItemWithAmount itemWithAmount = CHILD_VILLAGER_RARE_ITEMS[rareItemIndex];
				villager.dropItem(itemWithAmount.item, ThreadLocalRandom.current().nextInt(itemWithAmount.min, itemWithAmount.max + 1));
			}
		}
	}
}
