package villagerdrop.item;

import java.util.concurrent.ThreadLocalRandom;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import villagerdrop.init.CustomItems;
import villagerdrop.init.CustomTabs;
import villagerdrop.potion.ProbabilisticPotionEffect;

public class CustomFood extends ItemFood {
	/** Number of ticks to finish 'EnumAction'. *Default is 32. */
	int itemUseDuration;
	/** Potion effect of the food. */
	ProbabilisticPotionEffect probabilisticPotionEffects[];
	
	public CustomFood(String name, int foodPoint, int itemUseDuration, boolean isWolfFood, ProbabilisticPotionEffect... probabilisticPotionEffects) {
		super(foodPoint, 0.5F, isWolfFood);
		this.setUnlocalizedName(name);
		this.setRegistryName(name);
		this.itemUseDuration = itemUseDuration;
		this.probabilisticPotionEffects = probabilisticPotionEffects;
		this.setCreativeTab(CustomTabs.VILLAGERDROP_TAB);
		
		if(probabilisticPotionEffects.length > 0) {
			this.setAlwaysEdible();
		}

		CustomItems.ITEMS.add(this);
	}
	
    /**
     * Override this for add potion effect to player
     */
	@Override
    protected void onFoodEaten(ItemStack stack, World worldIn, EntityPlayer player) {
        if (!worldIn.isRemote && probabilisticPotionEffects != null) {
        	for(ProbabilisticPotionEffect effect : probabilisticPotionEffects) {
        		if(effect.triggerRate > ThreadLocalRandom.current().nextDouble()) {
        			player.addPotionEffect(new PotionEffect(effect)); // Create new effect to avoid modify original effect data
        		}
			}
        }
    }
	
	/**
	 * Override this for change itemUseDuration.
	 */
	@Override
	public int getMaxItemUseDuration(ItemStack stack) {
		return itemUseDuration;
	}
}
