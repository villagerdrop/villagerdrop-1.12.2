package villagerdrop.item;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import villagerdrop.init.CustomBlocks;
import villagerdrop.init.CustomItems;
import villagerdrop.init.CustomTabs;

public class CustomBlock extends Block {
	public CustomBlock(String name, Material materialIn, SoundType sound) {
		super(materialIn);
		this.setUnlocalizedName(name);
		this.setRegistryName(name);
		this.setCreativeTab(CustomTabs.VILLAGERDROP_TAB);
		this.setSoundType(sound);
		
		CustomBlocks.BLOCKS.add(this);
		CustomItems.ITEMS.add(new ItemBlock(this).setUnlocalizedName(name).setRegistryName(name));
	}
}