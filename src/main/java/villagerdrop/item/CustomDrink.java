package villagerdrop.item;

import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import villagerdrop.potion.ProbabilisticPotionEffect;

public class CustomDrink extends CustomFood {
	public CustomDrink(String name, int foodPoint, int itemUseDuration, ProbabilisticPotionEffect... probabilisticPotionEffect) {
		super(name, foodPoint, itemUseDuration, false, probabilisticPotionEffect);
	}
	
	@Override
    public EnumAction getItemUseAction(ItemStack stack) {
        return EnumAction.DRINK;
    }
}