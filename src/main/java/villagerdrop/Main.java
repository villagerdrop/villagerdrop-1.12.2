package villagerdrop;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import villagerdrop.handler.DropHandler;
import villagerdrop.init.CustomBlocks;
import villagerdrop.init.CustomItems;
import villagerdrop.util.Reference;
import villagerdrop.util.Registry;
import villagerdrop.util.VillagerDropConfig;

@Mod(modid = Reference.MODID, version = Reference.VERSION, name = Reference.NAME)
public class Main {
	@EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		// Register all effects, items, blocks......
		MinecraftForge.EVENT_BUS.register(new Registry());
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		// Handle villagerdrop.cfg
		MinecraftForge.EVENT_BUS.register(new VillagerDropConfig());
		
		// Handle all entity drops event
		MinecraftForge.EVENT_BUS.register(new DropHandler());
	}
	
	@EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		// Register all smelting
		CustomItems.registerSmelting();
		CustomBlocks.registerSmelting();
	}
}
