package villagerdrop.init;

import java.util.ArrayList;

import net.minecraft.init.MobEffects;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import villagerdrop.item.CustomDrink;
import villagerdrop.item.CustomFood;
import villagerdrop.potion.ProbabilisticPotionEffect;

public class CustomItems {
	/** All items for registry. */
	public static final ArrayList<Item> ITEMS = new ArrayList<Item>();
	
	public static final Item PIECE_OF_VILLAGER = new CustomFood("piece_of_villager", 2, 64, true, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.SLOWNESS, 20 * 20, 4, 1.0),
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 20 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.HUNGER, 5 * 20, 0, 0.2),
		new ProbabilisticPotionEffect(MobEffects.NAUSEA, 3 * 20, 0, 0.2),
	});
	
	public static final Item COOKED_PIECE_OF_VILLAGER = new CustomFood("cooked_piece_of_villager", 6, 32, true);
	
	public static final Item CHARRED_PIECE_OF_VILLAGER = new CustomFood("charred_piece_of_villager", 1, 16, false, new ProbabilisticPotionEffect[] {
			new ProbabilisticPotionEffect(MobEffects.BLINDNESS, 3 * 20, 0, 0.5),
		new ProbabilisticPotionEffect(MobEffects.SPEED, 20 * 20, 1, 0.4),
		new ProbabilisticPotionEffect(MobEffects.WEAKNESS, 30 * 20, 0, 0.3),
		new ProbabilisticPotionEffect(MobEffects.WITHER, 15 * 20, 0, 0.15),
	});
	
	public static final Item DICED_VILLAGER = new CustomFood("diced_villager", 1, 48, true, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.HUNGER, 5 * 20, 0, 0.5),
		new ProbabilisticPotionEffect(MobEffects.NAUSEA, 3 * 20, 0, 0.5),
	});
	
	public static final Item COOKED_DICED_VILLAGER = new CustomFood("cooked_diced_villager", 2, 16, true, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.SPEED, 10 * 20, 0, 0.25),
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 5 * 20, 0, 0.25),
		new ProbabilisticPotionEffect(MobEffects.HASTE, 10 * 20, 0, 0.25),
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 10 * 20, 0, 0.25),
	});
	
	public static final Item CHARRED_DICED_VILLAGER = new CustomFood("charred_diced_villager", -1, 16, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.BLINDNESS, 1 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.SPEED, 35 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.WITHER, 5 * 20, 0, 0.5),
	});
	
	public static final Item HAPPY_SAUSAGE = new CustomFood("happy_sausage", 2, 16, true, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 5 * 20, 0, 1.0),
	});
	
	public static final Item SIMULATED_APPLE = new CustomFood("simulated_apple", 5, 32, true, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.NAUSEA, 10 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.HUNGER, 7 * 20, 255, 1.0),
		new ProbabilisticPotionEffect(MobEffects.INVISIBILITY, 60 * 20, 0, 0.5),
	});
	
	public static final Item HAPPY_FACE = new CustomFood("happy_face", 1, 32, true, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.NAUSEA, 5 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.HUNGER, 2 * 20, 9, 0.33),
		new ProbabilisticPotionEffect(MobEffects.HASTE, 30 * 20, 2, 0.33),
		new ProbabilisticPotionEffect(MobEffects.JUMP_BOOST, 30 * 20, 2, 0.33),
		new ProbabilisticPotionEffect(MobEffects.SPEED, 30 * 20, 2, 0.33),
	});
	
	public static final Item TRICOLOR_MEATBALLS = new CustomFood("tricolor_meatballs", 4, 24, true, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.NAUSEA, 5 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 10 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 5 * 20, 2, 1.0),
	});
	
	public static final Item CRISPY_CUP = new CustomFood("crispy_cup", 0, 16, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.INVISIBILITY, 1 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.SPEED, 20 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.BLINDNESS, 3 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.WITHER, 1 * 20, 9, 1.0),
	});
	
	public static final Item HOT_COCOA = new CustomDrink("hot_cocoa", 1, 32, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 10 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.ABSORPTION, 25 * 20, 0, 1.0),
	});
	
	public static final Item HOT_COCOA_WITH_MARSHMALLOWS = new CustomDrink("hot_cocoa_with_marshmallows", 2, 32, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 15 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.ABSORPTION, 30 * 20, 0, 1.0),
	});
	
	public static final Item CRISPY_BAG = new CustomFood("crispy_bag", 0, 8, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.INVISIBILITY, 1 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.SPEED, 10 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.BLINDNESS, 2 * 20, 0, 1.0),
	});
	
	public static final Item BAG_OF_CHIPS = new CustomFood("bag_of_chips", 5, 32, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.SLOWNESS, 10 * 20, 0, 1.0),	
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 20 * 20, 0, 1.0),
	});
	
	public static final Item BAG_OF_BROCCOLI = new CustomFood("bag_of_broccoli", 2, 48, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(CustomPotions.TOO_HEALTHY, 30 * 20, 0, 1.0),
	});
	
	public static final Item BAG_OF_BROCCOLI_CHIPS = new CustomFood("bag_of_broccoli_chips", 6, 32, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.SLOWNESS, 10 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 20 * 20, 0, 1.0),
	});
	
	public static final Item FORTUNE_CRISPY = new CustomFood("fortune_crispy", 0, 8, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.LUCK, 30 * 20, 0, 0.25),
		new ProbabilisticPotionEffect(MobEffects.UNLUCK, 30 * 20, 0, 0.25),
	});
	
	public static final Item CANDY_CANE = new CustomFood("candy_cane", 1, 16, false, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.NAUSEA, 5 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 5 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.ABSORPTION, 5 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.GLOWING, 5 * 20, 0, 0.1),
	});
	
	public static final Item VILLAGER_JUICE = new CustomDrink("villager_juice", 5, 16, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 10 * 20, 0, 1.0),
	});
	
	public static final Item VILLAGER_ENERGY_DRINK = new CustomDrink("villager_energy_drink", 20, 32, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.SPEED, 30 * 20, 2, 1.0),
		new ProbabilisticPotionEffect(MobEffects.HASTE, 30 * 20, 1, 1.0),
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 30 * 20, 1, 1.0),
		new ProbabilisticPotionEffect(MobEffects.JUMP_BOOST, 30 * 20, 0, 1.0),
	});
	
	public static final Item VILLAGER_BEER = new CustomDrink("villager_beer", 8, 32, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 30 * 20, 1, 0.65),
		new ProbabilisticPotionEffect(MobEffects.RESISTANCE, 20 * 20, 1, 0.65),
		new ProbabilisticPotionEffect(MobEffects.FIRE_RESISTANCE, 30 * 20, 0, 0.65),
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 20 * 20, 1, 0.65),
	});
	
	public static final Item VILLAGER_WINE = new CustomDrink("villager_wine", 4, 32, new ProbabilisticPotionEffect[] {
		new ProbabilisticPotionEffect(MobEffects.REGENERATION, 40 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.RESISTANCE, 30 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.FIRE_RESISTANCE, 40 * 20, 0, 1.0),
		new ProbabilisticPotionEffect(MobEffects.STRENGTH, 30 * 20, 0, 1.0),
	});
	
	public static void registerSmelting() {
		GameRegistry.addSmelting(PIECE_OF_VILLAGER, new ItemStack(COOKED_PIECE_OF_VILLAGER, 1), 0.5F);
		GameRegistry.addSmelting(COOKED_PIECE_OF_VILLAGER, new ItemStack(CHARRED_PIECE_OF_VILLAGER, 1), 0.5F);
		GameRegistry.addSmelting(DICED_VILLAGER, new ItemStack(COOKED_DICED_VILLAGER, 1), 0.5F);
		GameRegistry.addSmelting(COOKED_DICED_VILLAGER, new ItemStack(CHARRED_DICED_VILLAGER), 0.5F);
	}
}
