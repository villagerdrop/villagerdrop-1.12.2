package villagerdrop.init;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import villagerdrop.item.CustomBlock;

public class CustomBlocks {
	/** All blocks for registry. */
	public static final ArrayList<CustomBlock> BLOCKS = new ArrayList<CustomBlock>();
	
	public static final Block BLOCK_OF_VILLAGER = new CustomBlock("block_of_villager", Material.SPONGE, SoundType.SLIME).setHardness(0.5F);
	
	public static final Block COOKED_BLOCK_OF_VILLAGER = new CustomBlock("cooked_block_of_villager", Material.SPONGE, SoundType.CLOTH).setHardness(0.4F);
	
	public static final Block CHARRED_BLOCK_OF_VILLAGER = new CustomBlock("charred_block_of_villager", Material.SPONGE, SoundType.METAL).setHardness(0.25F);
	
	public static void registerSmelting() {
		GameRegistry.addSmelting(BLOCK_OF_VILLAGER, new ItemStack(COOKED_BLOCK_OF_VILLAGER, 1), 0.5F);
		GameRegistry.addSmelting(COOKED_BLOCK_OF_VILLAGER, new ItemStack(CHARRED_BLOCK_OF_VILLAGER, 1), 0.5F);
	}
}
