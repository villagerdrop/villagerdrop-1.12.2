package villagerdrop.init;

import java.util.ArrayList;

import net.minecraft.potion.Potion;
import villagerdrop.potion.CustomPotion;
import villagerdrop.potion.TooHealthyPotion;

public class CustomPotions {
	/** All potions for registry. */
	public static final ArrayList<CustomPotion> POTIONS = new ArrayList<CustomPotion>();
	
	public static final Potion TOO_HEALTHY = new TooHealthyPotion("too_healthy", true, 6684570, 0, 0);
}