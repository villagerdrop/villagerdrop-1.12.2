package villagerdrop.init;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class CustomTabs {
	public static final CreativeTabs VILLAGERDROP_TAB = (new CreativeTabs("villagerdrop") {
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(CustomItems.PIECE_OF_VILLAGER);
		}
	});
}
