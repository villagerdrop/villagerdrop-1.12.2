package villagerdrop.util;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.potion.Potion;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import villagerdrop.init.CustomBlocks;
import villagerdrop.init.CustomItems;
import villagerdrop.init.CustomPotions;

public class Registry {	
	// Register potion
	@SubscribeEvent
	public void onEffectRegister(RegistryEvent.Register<Potion> event) {
		event.getRegistry().registerAll(CustomPotions.POTIONS.toArray(new Potion[0]));
	}
	
	// Register item
	@SubscribeEvent
	public void onItemRegister(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(CustomItems.ITEMS.toArray(new Item[0]));
	}
	
	// Register block
	@SubscribeEvent
	public void onBlockRegister(RegistryEvent.Register<Block> event) {
		event.getRegistry().registerAll(CustomBlocks.BLOCKS.toArray(new Block[0]));
	}
	
	// Register all model
	@SubscribeEvent
	public void onModelRegister(ModelRegistryEvent event) {
		for(Item item : CustomItems.ITEMS) {
			ModelLoader.setCustomModelResourceLocation(item , 0, new ModelResourceLocation(item.getRegistryName().toString()));
		}
	}
}
