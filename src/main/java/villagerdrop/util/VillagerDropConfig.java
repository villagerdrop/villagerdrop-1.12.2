package villagerdrop.util;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.Config.Comment;

@Config(modid = Reference.MODID)
public class VillagerDropConfig {
	@Comment({"Prevent villager drop item and xp when killed by a player."})
	public static boolean preventVillagerDropWhenKilledByPlayer = false;
}