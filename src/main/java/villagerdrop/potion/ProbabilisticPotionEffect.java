package villagerdrop.potion;

import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

/**
 * The potion effect which is not always trigger.
 */
public class ProbabilisticPotionEffect extends PotionEffect {
	/** The trigger rate of potion effect. range: [0.0 ~ 1.0]*/
	public double triggerRate;
	
	public ProbabilisticPotionEffect(Potion potion, int tick, int level, double triggerRate) {
		super(potion, tick, level);
		this.triggerRate = triggerRate;
	}
}