package villagerdrop.potion;

import net.minecraft.client.Minecraft;
import net.minecraft.potion.Potion;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import villagerdrop.init.CustomPotions;
import villagerdrop.util.Reference;

public class CustomPotion extends Potion {
	public static ResourceLocation INVENTORY_RESOURCE_LOCATION = new ResourceLocation(Reference.MODID, "textures/gui/container/inventory.png");
	
	public CustomPotion(String name, boolean isBadEffect, int liquidColor, int iconIndexX, int iconIndexY) {
		super(isBadEffect, liquidColor);
		this.setRegistryName(new ResourceLocation(Reference.MODID, name));
		this.setPotionName("effect." + name);
		super.setIconIndex(iconIndexX, iconIndexY);
		
		CustomPotions.POTIONS.add(this);
	}
    
	@SideOnly(Side.CLIENT)
	@Override
	public int getStatusIconIndex() {
		Minecraft.getMinecraft().renderEngine.bindTexture(INVENTORY_RESOURCE_LOCATION);
		return super.getStatusIconIndex();
	}
}