package villagerdrop.potion;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;
import villagerdrop.util.Reference;

public class TooHealthyPotion extends CustomPotion {
	public static ResourceLocation TOO_HEALTHY_RESOURCE_LOCATION = new ResourceLocation(Reference.MODID, "textures/gui/too_healthy_hud.png");
	
	public TooHealthyPotion(String name, boolean isBadEffect, int liquidColor, int iconIndexX, int iconIndexY) {
		super(name, isBadEffect, liquidColor, iconIndexX, iconIndexY);
	}
	
	// Render custom HUD if enableCustomHUD is true
	@Override
	public void renderHUDEffect(int x, int y, PotionEffect effect, net.minecraft.client.Minecraft mc, float alpha) {
		Minecraft.getMinecraft().renderEngine.bindTexture(TOO_HEALTHY_RESOURCE_LOCATION);
		int startPointOnScreenX = 0;
		int startPointOnScreenY = 0;
		int screenWidth = Minecraft.getMinecraft().displayWidth;
		int screenHeight = Minecraft.getMinecraft().displayHeight;
		int textureWidth = 256;
		int textureHeight = 256;
		Gui.drawModalRectWithCustomSizedTexture(startPointOnScreenX, startPointOnScreenY, 0, 0, screenWidth, screenHeight, textureWidth*screenWidth/screenHeight, textureHeight);
	}
}
