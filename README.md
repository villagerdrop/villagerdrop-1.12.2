<img src="https://gitlab.com/villagerdrop/villagerdrop-1.12.2/-/raw/main/images/preview.png"/>

Add some drop for villager!

Some foods have potion effects and have some probability to trigger.

### Common item:

- Emerald (0~2)

<img src="https://gitlab.com/villagerdrop/villagerdrop-1.12.2/-/raw/main/images/emerald.png" width="16" height="16"/>

- Piece of Villager (1~3)


<img src="https://gitlab.com/villagerdrop/villagerdrop-1.12.2/-/raw/main/src/main/resources/assets/villagerdrop/textures/items/piece_of_villager.png" width="16" height="16"/>

### Rare item: (You can loot some rare item if you are lucky, it will drop one kind at a time.)

- Villager® Juice (1~2)

<img src="https://gitlab.com/villagerdrop/villagerdrop-1.12.2/-/raw/main/src/main/resources/assets/villagerdrop/textures/items/villager_juice.png" width="16" height="16"/>

- Villager® Energy Drink (1~2)

<img src="https://gitlab.com/villagerdrop/villagerdrop-1.12.2/-/raw/main/src/main/resources/assets/villagerdrop/textures/items/villager_energy_drink.png" width="16" height="16"/>

- Villager® Beer (1~2)

<img src="https://gitlab.com/villagerdrop/villagerdrop-1.12.2/-/raw/main/src/main/resources/assets/villagerdrop/textures/items/villager_beer.png" width="16" height="16"/>

- Villager® Wine (1~2)

<img src="https://gitlab.com/villagerdrop/villagerdrop-1.12.2/-/raw/main/src/main/resources/assets/villagerdrop/textures/items/villager_wine.png" width="16" height="16"/>

## Development

- JDK: openjdk-8-jdk

- Api: forge-1.12.2-14.23.5.2859

### Setup environment

```
./gradlew genEclipseRuns
./gradlew eclipse
```

Edit "runClient.launch" and "runServer.launch" ${MC_VERSION} to 1.12.2

### Build

```
./gradlew bulid
```

The mod will output to "./build/libs/"
